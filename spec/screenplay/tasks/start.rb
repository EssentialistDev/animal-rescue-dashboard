class Start
  def self.withNoAvailableAnimals
    new
  end

  def perform_as(actor)
    actor.capability.reset
  end
end
