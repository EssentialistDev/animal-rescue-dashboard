class AddAnAnimal
  def self.called name
    new(name)
  end

  def initialize(name)
    @name = name
  end

  def perform_as(actor)
    actor.capability.add(@name)
  end
end
