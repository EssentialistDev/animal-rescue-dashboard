class TheAnimals
  def self.available(to:)
    new().perform_as(to)
  end

  def perform_as(actor)
    actor.capability.animals_available
  end
end
