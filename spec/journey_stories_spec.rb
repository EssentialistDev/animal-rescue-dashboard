require 'bundler/setup'
require 'require_all'

require 'screenplay_pattern'
require 'animal-rescue'
require_all 'spec/screenplay'

describe "Becca should" do
  app = AnimalRescue.new

  becca = Actor.named("Becca")
  becca.can(use_ruby(app))

  it "be able to add the first animal" do
    Given.the_actor(becca).was_able_to(Start.withNoAvailableAnimals())

    When.the_actor(becca).attempts_to(AddAnAnimal.called("Haribo"))

    expect(TheAnimals.available(to: becca)).to include("Haribo")
  end
end 
