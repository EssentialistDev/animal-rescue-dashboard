# Animal Rescue

![Animal Rescue Dashboard desktop design](assets/images/desktop-animal-rescue-dashboard-design.png)

A platform for keeping track of rescue animals, scheduling vet appointments (with notification reminders) and managing applications for becoming a foster carer or adopting an animal.
