class AnimalRescue
  def initialize
    @animals = []
  end

  def reset
    @animals = []
  end

  def add(animal)
    @animals << animal
  end

  def animals_available
    @animals
  end
end
