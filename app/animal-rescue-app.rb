require 'sinatra/base'

class AnimalRescueApp < Sinatra::Base

  get '/' do
    haml :home, format: :html5
  end

end
